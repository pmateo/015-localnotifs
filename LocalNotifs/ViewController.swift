//
//  ViewController.swift
//  LocalNotifs
//
//  Created by formador on 27/1/17.
//  Copyright © 2017 formador. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController, UNUserNotificationCenterDelegate {
    
    //Notificaciones Locales
        //content
        //trigger - calendar / interval / geofence
        //id único
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Registrar", style: .plain, target: self, action: #selector(registerLocal))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Schedule", style: .plain, target: self, action: #selector(scheduleLocal))
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func registerLocal() {
        let center = UNUserNotificationCenter.current()
        
        center.requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            if granted {
                print("Mensaje correcto")
            } else {
                print("Mensaje incorrecto")
            }
        }

    }
    
    func scheduleLocal() {
        let center = UNUserNotificationCenter.current()
        
        registerCategories()
        
        //Content
        let content = UNMutableNotificationContent()
        content.title = "Alarma para despertarnos"
        content.body = "Despiértate que hay mucho atasco y vas a llegar tarde"
        content.categoryIdentifier = "alarm"
        content.userInfo = ["customData": "fizzbuzz"]
        content.sound = UNNotificationSound.default()
        
        var dateComponents = DateComponents()
        dateComponents.hour = 9
        dateComponents.minute = 39
        
        //Trigger Calendario
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        //Trigger Intervalo
        let trigger2 = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        
        //Request
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger2)
        
        //center.removeAllPendingNotificationRequests()
        
        center.add(request)
    }
    
    //UNNotificationAction
        //Id
        //Title
        //Opciones
    
    func registerCategories() {
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        
        let show = UNNotificationAction(identifier: "show", title: "Muéstrame más", options: .foreground)
        let category = UNNotificationCategory(identifier: "alarm", actions: [show], intentIdentifiers: [])
        
        center.setNotificationCategories([category])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo
        
        if let customData = userInfo["customData"] as? String {
            print("Hemos recibido: \(customData)")
        }
        
        switch response.actionIdentifier {
        case UNNotificationDefaultActionIdentifier:
            //Swipe para Unlock
            print("El usuario ha deslizado la notificación")
            break
        case "show":
            //El usuario ha apretado nuestro botón
            print("Mostrar más información")
            break
        default:
            break
        }
        completionHandler()
    }

}

